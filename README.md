# Running the app locally

(from the project root directory)

Install server packages

```
npm install
```

Install client packages

```
cd client
```

```
npm install
```

```
cd ../
```

Start server

```
npm run dev
```

Server runs on port 5000 (http://localhost:5000)

Start client

```
npm run client
```

Server runs on port 3000 (http://localhost:3000)
