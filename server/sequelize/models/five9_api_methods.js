const Sequelize = require('sequelize');

module.exports = sequelize => {
  const options = {
    timestamps: false,
    createdAt: 'five9_api_methods',
    hasTrigger: false,
  };

  const definition = {
    ['id']: {
      allowNull: false,
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    ['name']: {
      allowNull: false,
      type: Sequelize.STRING(40),
    },
  };

  return sequelize.define('five9_api_methods', definition, options);
};
