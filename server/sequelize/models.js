const fs = require('fs');
const path = require('path');

_isModel = (fileName) => {
  if(fileName === 'relationships.js')
    return false;

  return fileName.endsWith('.js');
}

initializeModels = (sequelize) => {
  console.info(`BEGIN Models Init`);

  const modelsDir = path.join(__dirname, 'models');
  fs.readdirSync(modelsDir)
    .filter(_isModel)
    .forEach(fileName => {
      require(`${modelsDir}/${fileName}`)(sequelize);
    });
  require(`${modelsDir}/relationships.js`)(sequelize);

  console.info(`END Models Init`);
};

module.exports = initializeModels;
