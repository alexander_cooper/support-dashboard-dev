const Sequelize = require('sequelize');
const {getSecret} = require('../secrets');
const initializeModels = require('./models');

let cache = undefined;

_init = async () => {
  console.info(`BEGIN Sequelize Init`);
  const secretId = `senior_db_${process.env.ENV}`;
  const region = process.env.AWS_SECRET_REGION;
  const {host, dbname, username, password, engine} = await getSecret(
    region,
    secretId
  );

  console.info(`Connecting to DB: [${host}::${dbname}], Dialect: [${engine}]`);

  const options = {
    dialect: engine,
    host,
    timezone: 'US/Central',
    dialectOptions: {
      appName: 'support-dashboard',
      options: {
        useUTC: true,
      },
    },
    pool: {
      max: 20,
      min: 0,
      acquire: 10000,
      idle: 5000,
    },
  };

  const sequelize = new Sequelize(dbname, username, password, options);
  await sequelize.authenticate();
  initializeModels(sequelize);
  cache = sequelize;

  console.info(`END Sequelize Init`);
};

const getSequelize = async () => {
  if (!cache) await _init();
  return cache;
};

module.exports = getSequelize;
