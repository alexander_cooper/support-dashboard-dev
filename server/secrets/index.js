const AWS = require("aws-sdk");

/**
 *
 * @param region - the AWS Region
 * @param SecretId - the AWS secret id
 * @returns {Promise<>}
 * @description
 * Gets a secret from AWS
 */
const getSecret = async (region, SecretId) => {
  if (!SecretId) throw new Error("A secret id is required to get a secret");

  const secretsmanager = new AWS.SecretsManager({
    region,
  });

  try {
    const data = await secretsmanager.getSecretValue({ SecretId }).promise();

    const secretString = _extractSecretString(data);

    return JSON.parse(secretString);
  } catch (err) {
    throw err;
  }
};

/**
 *
 * @param {object} data
 * @returns {string} - The secret data as a JSON string.
 * @private
 * @description
 * Returns the secret information as stringified JSON.
 */
const _extractSecretString = (data) => {
  if ("SecretString" in data) {
    return data.SecretString;
  } else {
    const buff = new Buffer(data.SecretBinary, "base64");
    return buff.toString("ascii");
  }
};

module.exports = {
  getSecret,
};
