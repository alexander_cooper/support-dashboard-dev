const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const routes = require('../routes');

const init = () => {
  const app = express();
  app.use(bodyParser.json());
  app.use(express.static(path.resolve(__dirname, '../../client/build')));

  const pathNames = Object.keys(routes);
  pathNames.forEach((pathName) => {
    app.use(`/api/${pathName}`, routes[pathName]);
  });

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../client/build', 'index.html'));
  });
  return app;
};

module.exports = init;
