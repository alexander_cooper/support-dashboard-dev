const http = require('http');
const express = require('./express');

const init = () => {
  const app = express();
  const server = http.Server(app);
  return server;
};

module.exports = init;
