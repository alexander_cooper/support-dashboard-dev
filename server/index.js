require('dotenv').config();
const loaders = require('./loaders');
const PORT = process.env.SERVER_PORT || 5000;

const startServer = () => {
  const server = loaders();
  server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
};

startServer();
