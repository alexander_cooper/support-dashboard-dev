const reqHandler = (fn) => async ({body}, res) => {
  try {
    const response = await fn(body);
    res.status(response.status || 200).json(response);
  } catch (err) {
    console.error(`/controllers/reqHandler ${fn.name} %o`, err);
    res.status(err.status || 500).json(err);
  }
};

module.exports = {
  reqHandler,
};
