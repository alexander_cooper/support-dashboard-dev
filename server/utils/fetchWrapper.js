const fetch = require('node-fetch');

/**
 * @function fetchWrapper
 * @param {string} arg1 - REST method | url
 * @param {string} [url] - url
 * @param {Object} [body] - body of message
 * @description
 *   Wrapper for the fetch api that provides options defaults and base response code handling.
 * @return {Promise<Object>} A promise containing the deserialized response object.
 * */
const fetchWrapper = async (arg1, url, body) => {
  // if called with one argument, default to 'GET' method
  const _method = url ? arg1.toUpperCase() : 'GET';
  // if called without method arg, the first
  const _url = url || arg1;

  const options = {
    method: _method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: body && JSON.stringify(body), // body can be undefined, that's ok
  };
  const response = await fetch(_url, options);
  return await handleResponse(response);
};

/**
 * @function handleResponse
 * @param {Object} response - The response object.
 * @description
 *   A handler for the fetch response Object
 * @return {Promise<Object>} A promise containing the deserialized response object.
 * */
const handleResponse = async (response) => {
  if (response.status === 401) {
    throw new Error('Unauthorized');
  }

  const res = await response.json();

  if (response.status === 400) {
    throw new Error('Bad Request');
  }

  if (response.status === 403) {
    throw new Error('Forbidden');
  }

  if (response.status === 404) {
    throw new Error('Not Found');
  }

  if (response.status < 200 || response.status >= 300) {
    throw new Error(`Error. Response status: ${response.status}`);
  }
  return res;
};

module.exports = {
  fetchWrapper,
};
