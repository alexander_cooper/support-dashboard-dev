# base image
FROM node:14.7.0-alpine

# set working directory
RUN mkdir -p /app/client
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/client/node_modules/.bin:$PATH
ENV PATH /app/node_modules/.bin:$PATH

ARG AWS_SECRET_REGION
ENV AWS_SECRET_REGION=${AWS_SECRET_REGION}

# install and cache app dependencies
COPY . .

# This is an attempt to fix an issue when using alpine in ci
RUN npm config set unsafe-perm true

RUN npm install

WORKDIR /app/client

RUN npm install
RUN npm install react-scripts@3.4.1 -g --silent
RUN npm run build

WORKDIR /app
# start app
CMD ["npm", "start", "production"]