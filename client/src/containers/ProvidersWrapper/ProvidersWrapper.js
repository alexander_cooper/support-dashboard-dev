import React from 'react';
import {MuiThemeProvider, StylesProvider} from '@material-ui/core/styles';
import muiTheme from 'muiTheme';

const ProvidersWrapper = ({children}) => {
  return (
    <MuiThemeProvider theme={muiTheme}>
      <StylesProvider injectFirst>{children}</StylesProvider>
    </MuiThemeProvider>
  );
};

export default ProvidersWrapper;
