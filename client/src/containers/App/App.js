import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {AppBar, Home} from 'components';
import ProvidersWrapper from 'containers/ProvidersWrapper';
import useStyles from './styles';

const AppMain = ({children}) => {
  const classes = useStyles();

  return <main className={classes.content}>{children}</main>;
};

const App = () => {
  const classes = useStyles();
  return (
    <div className={classes.app}>
      <ProvidersWrapper>
        <Router>
          <AppBar />
          <AppMain>
            <Switch>
              <Route component={Home} />
            </Switch>
          </AppMain>
        </Router>
      </ProvidersWrapper>
    </div>
  );
};

export default App;
