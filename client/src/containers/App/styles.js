import {makeStyles} from '@material-ui/core/styles';
import {MENU_DRAWER_WIDTH, APP_BAR_HEIGHT} from '../../constants/STYLES';

export default makeStyles((theme) => ({
  app: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    height: `calc(100vh - ${APP_BAR_HEIGHT}px)`,
    width: '100%',
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    //marginRight: MENU_DRAWER_WIDTH,
    marginTop: APP_BAR_HEIGHT,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 0,
  },
  drawer: {
    width: MENU_DRAWER_WIDTH,
    //flexShrink: 0,
  },
  drawerPaper: {
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: APP_BAR_HEIGHT,
    width: MENU_DRAWER_WIDTH,
  },
}));
