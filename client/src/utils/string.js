export const hasLength = (str) => {
  return str && str.length > 0;
};

export const filterNonNumeric = (str) => {
  return str.replace(/\D/g, '');
};
