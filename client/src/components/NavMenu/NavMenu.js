import React, {useEffect, useReducer} from 'react';
import {Link} from 'react-router-dom';
import {
  Avatar,
  Collapse,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Tooltip,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {ExpandLess, ExpandMore, Home, Menu} from '@material-ui/icons';
import reducer from '../../reducers/NavBar';

const useStyles = makeStyles((theme) => ({
  list: {
    width: 300,
  },
  nestedList: {
    paddingLeft: theme.spacing(4),
  },
}));

const _getMenuItems = () => {
  return [
    {
      name: 'homepage',
      displayName: 'Homepage',
      icon: Home,
      linkTo: '/',
    },
  ];
};

const initialState = {
  isNavMenuOpen: false,
  menuItems: [],
  menuItemsExpandState: _getMenuItems().reduce((acc, {name}) => {
    acc[name] = true;
    return acc;
  }, {}),
};

const NavMenu = () => {
  const classes = useStyles();

  const [state, dispatch] = useReducer(reducer, initialState);
  const {isNavMenuOpen, menuItems, menuItemsExpandState} = state;

  useEffect(() => {
    dispatch({
      type: 'NAV_MENU_ITEMS_SET',

      menuItems: _getMenuItems(),
    });
  }, []);

  const _handleNavMenuClick = (expandMenuItemName) => ({key, type}) => {
    if (type === 'keydown' && (key === 'Tab' || key === 'Shift')) return;

    if (expandMenuItemName) {
      dispatch({
        type: 'NAV_MENU_ITEM_EXPAND_TOGGLE',
        expandMenuItemName,
      });
    }
    dispatch({
      type: 'NAV_MENU_OPEN_SET',
      isNavMenuOpen: true,
    });
  };

  const _handleNavMenuClose = () => {
    dispatch({
      type: 'NAV_MENU_OPEN_SET',
      isNavMenuOpen: false,
    });
  };

  const _buildMenu = () => (
    <div className={classes.list} role="presentation">
      <List>
        {menuItems.map((menuItem, index) => {
          const Icon = menuItem.icon;
          return (
            <div key={`menuItem__${menuItem.name}__container`}>
              <ListItem
                button
                onClick={
                  menuItem.nestedItems
                    ? _handleNavMenuClick(menuItem.name)
                    : _handleNavMenuClose
                }
                component={menuItem.linkTo ? Link : undefined}
                to={menuItem.linkTo}
              >
                {Icon && (
                  <ListItemIcon>
                    <Icon />
                  </ListItemIcon>
                )}
                <ListItemText primary={menuItem.displayName} />
                {menuItem.nestedItems &&
                  (menuItemsExpandState[menuItem.name] ? (
                    <ExpandLess />
                  ) : (
                    <ExpandMore />
                  ))}
              </ListItem>
              {menuItem.nestedItems && (
                <Collapse
                  in={menuItemsExpandState[menuItem.name]}
                  timeout="auto"
                  unmountOnExit
                >
                  <List component="div" disablePadding>
                    {menuItem.nestedItems.map((nestedItem) => {
                      const Icon = nestedItem.icon;
                      const ActionIcon = nestedItem.actionIcon;
                      return (
                        <ListItem
                          button
                          onClick={_handleNavMenuClose}
                          className={classes.nestedList}
                          key={`menuItem__${menuItem.name}__${nestedItem.name}`}
                          component={nestedItem.linkTo ? Link : undefined}
                          to={nestedItem.linkTo}
                        >
                          {Icon && (
                            <ListItemAvatar>
                              <Avatar>
                                <Icon fontSize="small" />
                              </Avatar>
                            </ListItemAvatar>
                          )}
                          <ListItemText primary={nestedItem.displayName} />
                          {ActionIcon && (
                            <ListItemSecondaryAction>
                              <IconButton
                                edge="end"
                                aria-label={nestedItem.actionIconLabel}
                                onClick={nestedItem.actionIconOnClick}
                              >
                                <ActionIcon fontSize="small" />
                              </IconButton>
                            </ListItemSecondaryAction>
                          )}
                        </ListItem>
                      );
                    })}
                  </List>
                </Collapse>
              )}
              {index === menuItems.length - 1 ? null : <Divider />}
            </div>
          );
        })}
      </List>
    </div>
  );

  return (
    <>
      <Tooltip title="Navigation Menu">
        <IconButton
          edge="start"
          color="inherit"
          aria-label="menu"
          onClick={_handleNavMenuClick()}
        >
          <Menu />
        </IconButton>
      </Tooltip>
      <Drawer anchor="right" open={isNavMenuOpen} onClose={_handleNavMenuClose}>
        {_buildMenu()}
      </Drawer>
    </>
  );
};

export default NavMenu;
