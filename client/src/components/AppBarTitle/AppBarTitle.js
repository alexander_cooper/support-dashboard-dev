import React from 'react';
import {Link} from 'react-router-dom';
import {Typography} from '@material-ui/core';
import {Breadcrumbs} from 'components/common';
import {APP_NAME} from 'constants/index';
import './AppBarTitle.css';

const AppBarTitle = () => {
  return (
    <Typography variant="h6" className="appBarTitle">
      <Link to="/" className="appBarTitle__base">
        {APP_NAME}
      </Link>
      <Breadcrumbs />
    </Typography>
  );
};

export default AppBarTitle;
