import React from 'react';
import {Link} from 'react-router-dom';
import {AppBar, IconButton, Toolbar} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import AppBarTitle from '../AppBarTitle';
import NavMenu from '../NavMenu';
import {SqRingsIcon} from '../common';
import './AppBar.css';

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
}));

export default () => {
  const classes = useStyles();

  return (
    <div className="appBar">
      <AppBar position="fixed" className={classes.appBar} color="secondary">
        <Toolbar variant="dense">
          <Link to="/">
            <IconButton className="appBar__homeIcon">
              <SqRingsIcon height="35" />
            </IconButton>
          </Link>
          <AppBarTitle />
          <NavMenu />
        </Toolbar>
      </AppBar>
    </div>
  );
};
