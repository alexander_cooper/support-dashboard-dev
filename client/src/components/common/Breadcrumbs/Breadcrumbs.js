import React from 'react';
import {withRouter} from 'react-router';

const LEGEND = {
  flowroute: 'Flowroute',
};

const Separator = () => {
  return <span> &#x2219; </span>;
};

const Breadcrumbs = ({location}) => {
  const _getBreadcrumb = (dir) => {
    if (LEGEND[dir]) {
      return LEGEND[dir];
    }
    return dir;
  };

  const pathDirs = location.pathname.split('/').filter((part) => part.length);
  const breadcrumbs = pathDirs.map((dir) => _getBreadcrumb(dir));

  return (
    <span>
      {breadcrumbs.length > 0 && <span> | </span>}
      {breadcrumbs.map((breadcrumb, index) => {
        const _separator = index !== 0 ? <Separator /> : '';
        return (
          <span key={`Breadcrumb-${index}`}>
            {_separator}
            {breadcrumb}
          </span>
        );
      })}
    </span>
  );
};

const BreadcrumbsWithRouter = withRouter(Breadcrumbs);

export default BreadcrumbsWithRouter;
