import React from 'react';
import {Redirect, Route} from 'react-router-dom';
//import {isAuthenticated} from 'utils/authentication';

const isAuthenticated = () => true;

const ProtectedRoute = ({component: Component, render, ...routeProps}) => {
  const _renderFn = (props) => {
    if (!isAuthenticated() === true)
      return (
        <Redirect
          to={{pathname: '/login', state: {referrer: props.location}}}
        />
      );
    return render ? render() : <Component {...props} />;
  };
  return <Route {...routeProps} render={_renderFn} />;
};

export default ProtectedRoute;
