export const APP_BAR_HEIGHT = 48;

export const COLORS = {
  cerulean: '#01A1B7',
  teal: '#017788',
};

export const MENU_DRAWER_WIDTH = 400;
