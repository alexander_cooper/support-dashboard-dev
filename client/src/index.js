import React from 'react';
import ReactDOM from 'react-dom';
import toastr from 'toastr';
import App from 'containers/App';
import * as serviceWorker from './serviceWorker';
import './index.css';
import 'toastr/build/toastr.css';
toastr.options = {
  positionClass: 'toast-bottom-center',
};

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
