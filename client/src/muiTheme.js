import {createMuiTheme} from '@material-ui/core/styles';
import {COLORS} from 'constants/STYLES';

const theme = {
  typography: {
    fontFamily: 'Open Sans, sans-serif',
  },
  palette: {
    primary: {
      main: COLORS.teal,
    },
    secondary: {
      main: COLORS.cerulean,
    },
  },
  overrides: {
    '& .MuiFormControl-root': {
      margin: 2,
      minWidth: 120,
      maxWidth: 300,
    },
  },
};

export default createMuiTheme(theme);
