export default (state, action) => {
  switch (action.type) {
    case 'CONFIRMATION_OPEN':
      return {
        ...state,
        isConfirmOpen: true,
        deleteViewName: action.deleteViewName,
      };
    case 'CONFIRMATION_CLOSE':
      return {...state, isConfirmOpen: false, deleteViewName: ''};
    case 'NAV_MENU_ITEM_EXPAND_TOGGLE':
      const {expandMenuItemName} = action;
      return {
        ...state,
        menuItemsExpandState: {
          ...state.menuItemsExpandState,
          [expandMenuItemName]: !state.menuItemsExpandState[expandMenuItemName],
        },
      };
    case 'NAV_MENU_ITEMS_SET':
      return {...state, menuItems: action.menuItems};
    case 'NAV_MENU_OPEN_SET':
      return {...state, isNavMenuOpen: action.isNavMenuOpen};
    default:
      return state;
  }
};
