/**
 * @function fetchWrapper
 * @param {string} arg1 - REST method | url
 * @param {string} [url] - url
 * @param {Object} [body] - body of message
 * @param {Object} [headers] - additional HTTP headers
 * @description
 *   Wrapper for the fetch api that provides options defaults and base response code handling.
 * @return {Promise<Object>} A promise containing the deserialized response object.
 * */
export const fetchWrapper = async (arg1, url, body, additionalOptions) => {
  // if called with one argument, default to 'GET' method
  const _method = url ? arg1.toUpperCase() : 'GET';
  // if called without method arg, the first
  const _url = url || arg1;

  const options = {
    method: _method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: body && JSON.stringify(body), // body can be undefined, that's ok
    ...additionalOptions,
  };

  try {
    const response = await fetch(_url, options);
    return await _handleResponse(response);
  } catch (err) {
    console.error(err);
    return err;
  }
};

/**
 * @function handleResponse
 * @param {Object} response - The response object.
 * @description
 *   A handler for the fetch response Object
 * @return {Promise<Object>} A promise containing the deserialized response object.
 * */
const _handleResponse = async (response) => {
  const {status} = response;
  if (status === 200) {
    return await response.json();
  }

  try {
    const {message, statusText} = await response.json();
    return console.error(message, statusText);
  } catch (err) {
    console.error(err);
    if (status === 400) {
      throw new Error('Bad request');
    }
    if (status === 401) {
      throw new Error('Unauthorized');
    }
    if (status === 403) {
      throw new Error('Forbidden');
    }
    if (status === 404) {
      throw new Error('Not Found');
    }
    throw new Error('Unknown error');
  }
};
